# How to begin career
## Know your tools
- <https://www.sketchapp.com/>
	- <https://www.sketchapp.com/docs/>
	- <https://github.com/diessica/awesome-sketch>
- Adobe suite
	- <http://www.adobe.com/fi/products/photoshop.html>
	- <http://www.adobe.com/fi/products/xd.html>
		- <https://www.xdguru.com/adobe-xd-guide/>
- Prototyping
	- <https://www.invisionapp.com/>, <https://www.invisionapp.com/craft>


## Evolve by doing
- Aim for entry level job building or designing websites
	- Aim to stay at leas a year

## How to get the first job
-  Aim for portfolio with 4 best projects
	- Plus points if projects are in use
		- Examples: Student guild websites or other bro bono work

### Checklist for getting started
- Pick your first project
- Find at least 10 similar designs and collect them in your design app to use as reference
	- Use inspiration sites listed below
- Write down or mindmap the structure :  What are MVP requirements


## Follow these sources
### Newsletters
- <https://sidebar.io/>
- <https://web-design-weekly.com/>
- <http://designweekly.atomic.io/archive/> 
- <https://blog.marvelapp.com/>

More here: 
<https://github.com/vredniy/awesome-newsletters>
### Other
- <https://github.com/gztchan/awesome-design>
- <https://github.com/nicolesaidy/awesome-web-design>

## Inspiration websites
Start your designs by collecting references from these sites:
 	- But always know your why. These sites are more style over substance 
- <https://dribbble.com/>
- <https://www.behance.net/>
- <https://www.siteinspire.com/>
—-
More:
- <https://usepanda.com/app>

## Ideas
Generate ideas based on others ideas.
- <https://www.producthunt.com/>

## Books
- Sprint - Jake Knapp
- Hooked - Nir Eyal
- Lean UX -  Jeff Gothelf
How to design you learn by doing but you can improve your process and impact:

More:
- <https://www.designbetter.co/>
- <https://uxtools.co/library>

## Design rules
- <http://www.goodui.org/>
- <https://zurb.com/triggers>
- <https://www.useronboard.com/>
- <https://www.designbetter.co/principles-of-product-design> 

## Other resources
- <https://github.com/alexpate/awesome-design-systems>
- See for topics to learn (Curriculum): <https://www.springboard.com/workshops/ux-design>

# More resources

## Articles
- <https://uxtools.co/guides/switching-careers-to-ux>
- <https://medium.com/ux-power-tools/a-step-by-step-guide-for-starting-a-new-app-design-project-in-sketch-469df0f24af8>
- <https://medium.com/ux-power-tools/100-questions-designers-always-ask-8b9f441bcd35>
- <https://medium.com/inspiration-supply/wireframes-by-top-ux-designers-d6922d34ddb8>
- <https://www.uxpin.com/studio/ebooks/guide-to-wireframing/>
- <https://uxplanet.org/the-subtle-art-that-differentiates-good-designers-from-great-designers-1ad3557b4c4>
- <https://medium.com/@kamushken/my-rules-of-solid-design-system-4a7fe6c1811>
- <https://medium.com/@kamushken/the-advantages-of-interactive-prototyping-855203728b83>

## Other tools
- <http://fontba.se/>
- <http://createwithflow.com/>
- <https://ludus.one>


### Utilize toolkits to get base from good practices
- <https://ui8.net/>
- <https://sketchrepo.com/tag/free-sketch-ui-kits/>
- <https://freebiesupply.com/blog/must-have-sketch-templates-for-new-ux-designers/>

#### individual kits
- <https://ui8.net/products/quantum-ui-kit>
- <https://ui8.net/products/unit-kit-2>
- <https://getbaseui.com/>
- <https://www.uxpower.tools/web/>
- <http://framesforsketch.com/>
- <https://visualhierarchy.co/shop/product/baikal-stylish-web-component-based-ui-kit/?utm_source=50off>
- <https://dribbble.com/shots/3344506-FREE-WEB-UI-Kit-symbols-styles-Freebie>
- <https://ui8.net/products/resource>
- <https://ui8.net/products/module-01-ui-kit>
- <https://ui8.net/products/module-02-layouts>

### Web app kits
- <https://ui8.net/products/web-interface-1>
- <https://ui8.net/products/dashboards-ui-kit>

### Based on front-end framwork
- <https://dribbble.com/shots/3725260-Material-Design-UI-Kit-Free>
- <https://github.com/ZTfer/Bootstrap-V4-Uikit>
- <https://www.froala.com/design-blocks>
- <https://github.com/joachimesque/Bulma.sketch>

### Wireframe kits

- <https://platforma.ws>
- <https://lstore.graphics/uxflow/>
- <https://ui8.net/category/wireframe-kits>
- <https://ui8.net/products/method-wireframe-kit>

- Small library: <https://ui8.net/products/connect-ux-kit>


## Style kits
<https://frontify.com/>